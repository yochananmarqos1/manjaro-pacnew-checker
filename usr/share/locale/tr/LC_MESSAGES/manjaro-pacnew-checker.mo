��    
      l       �       �   �   �   
   �  -   �  %   �  A   �  *   )  �  T     -  O   M    �  �   �     �     �  $   �  H   �  4   $  %  Y  .   	  I   �	   <span foreground='red'>New Pacnew/Pacsave files found:\n</span>
Keep in mind:\n
You must be aware of your choices. If you are unsure please inquire using our social channels such as our support forum.\n Do nothing Keep the original and remove the $suffix file Management of Pacnew/Pacsave complete Now we will <span foreground='red'>remove</span> the $suffix file Replace the original with the $suffix file This program is free software, you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Author: Stefano Capitani
stefano@manjaro.org View and merge the $suffix file What do you want to do with the file <span foreground='red'>$basefile</span>?\n Project-Id-Version: manjaro-pacnew-checker
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 <span foreground='red'>Yeni pacnew/pacsave dosyaları bulundu:</span>
Unutmayın:
Seçimlerinizden haberdar olmalısınız. Emin değilseniz, destek forumumuz gibi sosyal kanallarımızı kullanarak soruşturabilirsiniz. Hiçbir şey yapma $dosya son ekini kaldır pacnew/pacsave yönetimi tamamlandı Şimdi $suffix dosyasını <span foreground='red'>kaldıracağız</span> Orijinal dosya ile $suffix dosyasını değiştirin. Bu program ücretsiz yazılımdır, GNU Genel Kamu Lisansı'nın Free Software Foundation tarafından yayımlanan sürüm 3'ünün koşulları altında yeniden dağıtılması ve/veya değiştirilmesi mümkündür. Bu program kullanışlı olacağı umuduyla dağıtılmaktadır ancak herhangi bir garanti verilmeksizin, ticari amaçlı kullanım veya belirli bir amaca uygunluk garantisi dahil olmak üzere herhangi bir garanti verilmeksizin. Daha fazla ayrıntı için GNU Genel Kamu Lisansına bakın.

Yazar: Stefano Capitani
Stefano@manjaro.org $suffix dosyasını görüntüle ve birleştir <span foreground='red'>$basefile</span> dosyasını ne yapmak istiyorsun? 