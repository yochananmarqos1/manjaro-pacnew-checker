��    
      l       �       �   �   �   
   �  -   �  %   �  A   �  *   )  �  T     -  O   M    �  �   �     �  7   �  (   �  C     2   V  �  �  $   d	  M   �	   <span foreground='red'>New Pacnew/Pacsave files found:\n</span>
Keep in mind:\n
You must be aware of your choices. If you are unsure please inquire using our social channels such as our support forum.\n Do nothing Keep the original and remove the $suffix file Management of Pacnew/Pacsave complete Now we will <span foreground='red'>remove</span> the $suffix file Replace the original with the $suffix file This program is free software, you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Author: Stefano Capitani
stefano@manjaro.org View and merge the $suffix file What do you want to do with the file <span foreground='red'>$basefile</span>?\n Project-Id-Version: manjaro-pacnew-checker
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 <span foreground='red'>Novos arquivos pacnew/pacsave encontrados:</span>\n
Tenha em mente:\n
Você deve estar ciente de suas escolhas. Se você tiver dúvidas, por favor, pergunte usando nossos canais sociais, como o nosso fórum de suporte.\n Não fazer nada Mantenha o arquivo original e remova o arquivo $suffix. Gerenciamento de pacnew/pacsave completo Agora vamos <span foreground='red'>remover</span> o arquivo $suffix Substitua o arquivo original pelo arquivo $suffix. Este programa é software livre, você pode redistribuí-lo e/ou modificá-lo sob os termos da Licença Pública Geral GNU publicada pela Free Software Foundation, versão 3 da licença.

Este programa é distribuído na esperança de que seja útil, mas SEM QUALQUER GARANTIA, sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou ADEQUAÇÃO A UM DETERMINADO PROPÓSITO. Veja a Licença Pública Geral GNU para mais detalhes.

Autor: Stefano Capitani
stefano@manjaro.org Visualize e mescle o arquivo $suffix O que você quer fazer com o arquivo <span foreground='red'>$basefile</span>? 