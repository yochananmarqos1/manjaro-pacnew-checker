��    
      l       �       �   �   �   
   �  -   �  %   �  A   �  *   )  �  T     -  O   M    �  �   �     �  (   �  '   �       #     �  C     7	  D   S	   <span foreground='red'>New Pacnew/Pacsave files found:\n</span>
Keep in mind:\n
You must be aware of your choices. If you are unsure please inquire using our social channels such as our support forum.\n Do nothing Keep the original and remove the $suffix file Management of Pacnew/Pacsave complete Now we will <span foreground='red'>remove</span> the $suffix file Replace the original with the $suffix file This program is free software, you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Author: Stefano Capitani
stefano@manjaro.org View and merge the $suffix file What do you want to do with the file <span foreground='red'>$basefile</span>?\n Project-Id-Version: manjaro-pacnew-checker
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 <span foreground='red'>Nowe pliki pacnew/pacsave zostały znalezione:\n</span>
Pamiętaj:\n
Musisz być świadomy swoich wyborów. Jeśli masz wątpliwości, skorzystaj z naszych kanałów społecznościowych, takich jak nasze forum wsparcia.\n Nic nie robić Zachowaj oryginał i usuń plik $suffix. Zarządzanie pacnew/pacsave zakończone Teraz usuniemy plik $suffix. Zastąp oryginalny plikiem $suffix. Ten program jest wolnym oprogramowaniem, możesz go rozpowszechniać i/lub modyfikować na warunkach licencji GNU General Public License wydanej przez Free Software Foundation w wersji 3 licencji.

Ten program jest rozpowszechniany w nadziei, że będzie użyteczny, ale bez jakiejkolwiek gwarancji, nawet bez gwarancji przydatności handlowej lub przydatności do określonego celu. Aby uzyskać więcej szczegółów, zobacz GNU General Public License.

Autor: Stefano Capitani
Stefano@manjaro.org Zobacz i scal plik $suffix. Co chcesz zrobić z plikiem <span foreground='red'>$basefile</span>? 