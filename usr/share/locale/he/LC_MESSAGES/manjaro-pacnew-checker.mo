��    
      l       �       �   �   �   
   �  -   �  %   �  A   �  *   )  �  T     -  O   M    �    �     �  9   �  )   #  /   M  2   }  i  �  %   
  U   @
   <span foreground='red'>New Pacnew/Pacsave files found:\n</span>
Keep in mind:\n
You must be aware of your choices. If you are unsure please inquire using our social channels such as our support forum.\n Do nothing Keep the original and remove the $suffix file Management of Pacnew/Pacsave complete Now we will <span foreground='red'>remove</span> the $suffix file Replace the original with the $suffix file This program is free software, you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Author: Stefano Capitani
stefano@manjaro.org View and merge the $suffix file What do you want to do with the file <span foreground='red'>$basefile</span>?\n Project-Id-Version: manjaro-pacnew-checker
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 <span foreground='red'>נמצאו קבצים חדשים של pacnew/pacsave:</span>
תשעה לזכור:
עליך להיות מודע לבחירותיך. אם אינך בטוח, אנא שאל באמצעות ערוצי החברתיים שלנו, כגון פורום התמיכה שלנו. אל תעשה שום דבר שמור את המקור והסר את קובץ ה$suffix ניהול של pacnew/pacsave הושלם עכשיו נהסיר את קובץ ה־$suffix החלפת את המקור עם הקובץ $suffix התוכנית הזו היא תוכנה חופשית, אתה יכול להפצות אותה ו/או לשנות אותה תחת תנאי הרישיון הציבורי הכללי של GNU כפי שפורסם על ידי קרן התוכנה החופשית גרסה 3 של הרישיון.

התוכנית מופצת בתקווה שתהיה שימושית, אבל ללא אחריות כלשהי, אף ללא אחריות מסחרית או תאימות למטרה מסוימת. ראה את הרישיון הציבורי הכללי של GNU לפרטים נוספים.

יוצר: סטפנו קפיטאני
stefano@manjaro.org הצג ומזג את קובץ $suffix מה ברצונך לעשות עם הקובץ <span foreground='red'>$basefile</span>? 