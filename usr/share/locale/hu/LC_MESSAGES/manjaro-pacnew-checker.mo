��    
      l       �       �   �   �   
   �  -   �  %   �  A   �  *   )  �  T     -  O   M    �  �   �     k  B   �  (   �  F   �  2   3    f  /   y	  G   �	   <span foreground='red'>New Pacnew/Pacsave files found:\n</span>
Keep in mind:\n
You must be aware of your choices. If you are unsure please inquire using our social channels such as our support forum.\n Do nothing Keep the original and remove the $suffix file Management of Pacnew/Pacsave complete Now we will <span foreground='red'>remove</span> the $suffix file Replace the original with the $suffix file This program is free software, you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Author: Stefano Capitani
stefano@manjaro.org View and merge the $suffix file What do you want to do with the file <span foreground='red'>$basefile</span>?\n Project-Id-Version: manjaro-pacnew-checker
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 <span foreground='red'>Új pacnew/pacsave fájlok találhatók:</span>
Ne feledje:
Tudatosan kell döntenie. Ha bizonytalan, kérjen tájékoztatást a támogatási fórumainkon keresztül. Semmit sem csinálni Tartsd meg az eredeti fájlt, és távolítsd el a $suffix fájlt. Pacnew/pacsave kezelése befejeződött. Mostani <span foreground='red'>eltávolítjuk</span> a $suffix fájlt. Az eredeti fájlt cserélje le a $suffix fájllal. Ez a program szabad szoftver, amit átadhat és/vagy módosíthat a szabad szoftver alapítvány által kiadott GNU Általános Közreadási Feltételek alapján, a 3. verzió szerint.

Ez a program reményekkel adódik, hogy hasznos lesz, de garanciát nem vállalunk rá, sem kifejezett, sem beépített kereskedelmi célú alkalmazhatóságra vagy egy adott célra való alkalmazhatóságra vonatkozóan. További részletekért lásd a GNU Általános Közreadási Feltételeket.

Szerző: Stefano Capitani
Stefano@manjaro.org Nézze meg és olvassza össze a $suffix fájlt Mi akarod csinálni a <span foreground='red'>$basefile</span> fájllal? 