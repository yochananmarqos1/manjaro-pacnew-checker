ή    
      l       Ό       Ό   Κ   ½   
     -     %   Α  A   η  *   )  Ψ  T     -  O   M        ­     Ι  F   θ     /  M   L  %     4  ΐ  )   υ	  s   
   <span foreground='red'>New Pacnew/Pacsave files found:\n</span>
Keep in mind:\n
You must be aware of your choices. If you are unsure please inquire using our social channels such as our support forum.\n Do nothing Keep the original and remove the $suffix file Management of Pacnew/Pacsave complete Now we will <span foreground='red'>remove</span> the $suffix file Replace the original with the $suffix file This program is free software, you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Author: Stefano Capitani
stefano@manjaro.org View and merge the $suffix file What do you want to do with the file <span foreground='red'>$basefile</span>?\n Project-Id-Version: manjaro-pacnew-checker
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 <span foreground='red'>μλ‘μ΄ pacnew/pacsave νμΌμ΄ λ°κ²¬λμμ΅λλ€:\n</span>
μ£Όμν΄μΌ ν  μ :\n
μ ννλ κ²μ λν΄ μ μκ³  μμ΄μΌ ν©λλ€. λ§μ½ νμ€νμ§ μλ€λ©΄, μ°λ¦¬μ μ§μ ν¬λΌκ³Ό κ°μ μμ μ±λμ ν΅ν΄ λ¬Έμνμ­μμ€.\n μλ¬΄κ²λ νμ§ λ§μΈμ. $νμΌ μ΄λ¦μ μ λ―Έμ¬λ₯Ό μ μ§νκ³  μλ³Έμ μ κ±°νμΈμ. pacnew/pacsave κ΄λ¦¬ μλ£ μ΄μ  $suffix νμΌμ <span foreground='red'>μ κ±°</span>νκ² μ΅λλ€. $νμΌλ‘ μλ³Έμ λμ²΄νμΈμ. μ΄ νλ‘κ·Έλ¨μ λ¬΄λ£ μννΈμ¨μ΄μλλ€. λΉμ μ μμ  μννΈμ¨μ΄ μ¬λ¨μμ λ°νν GNU μΌλ° κ³΅μ€ μ¬μ© νκ°μμ μ‘°κ±΄μ λ°λΌ μ¬λ°°ν¬νκ±°λ/λλ μμ ν  μ μμ΅λλ€. λΌμ΄μ μ€μ λ²μ  3μλλ€.

μ΄ νλ‘κ·Έλ¨μ μ μ©νκ² μ¬μ©λ  κ²μ΄λΌλ ν¬λ§μΌλ‘ λ°°ν¬λμ§λ§, μ΄λ ν λ³΄μ¦λ μμΌλ©°, νΉμ  λͺ©μ μ λν μ½μ λ ν¬ν¨νμ§ μμ΅λλ€. μμΈν λ΄μ©μ GNU μΌλ° κ³΅μ€ μ¬μ© νκ°μλ₯Ό μ°Έμ‘°νμ­μμ€.

μ μ: μ€ννλΈ μΉ΄νΌνλ
stefano@manjaro.org $suffix νμΌμ λ³΄κ³  λ³ν©νμΈμ. λΉ¨κ°μμΌλ‘ νμλ <span foreground='red'>$basefile</span> νμΌμ μ΄λ»κ² μ¬μ©νκ³  μΆμΌμ κ°μ? 