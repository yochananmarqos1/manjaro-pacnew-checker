��    
      l       �       �   �   �   
   �  -   �  %   �  A   �  *   )  �  T     -  O   M    �    �     �  6   �  &     @   .  4   o  �  �  -   �	  F   �	   <span foreground='red'>New Pacnew/Pacsave files found:\n</span>
Keep in mind:\n
You must be aware of your choices. If you are unsure please inquire using our social channels such as our support forum.\n Do nothing Keep the original and remove the $suffix file Management of Pacnew/Pacsave complete Now we will <span foreground='red'>remove</span> the $suffix file Replace the original with the $suffix file This program is free software, you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Author: Stefano Capitani
stefano@manjaro.org View and merge the $suffix file What do you want to do with the file <span foreground='red'>$basefile</span>?\n Project-Id-Version: manjaro-pacnew-checker
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 <span foreground='red'>Fișiere noi pacnew/pacsave găsite:\n</span>
Țineți minte:\n
Trebuie să fiți conștienți de alegerile pe care le faceți. Dacă nu sunteți siguri, vă rugăm să întrebați folosind canalele noastre sociale, cum ar fi forumul nostru de suport.\n Nu face nimic Păstrează originalul și elimină fișierul $suffix. Gestionarea completă a pacnew/pacsave Acum vom <span foreground='red'>elimina</span> fișierul $suffix Înlocuiți fișierul original cu fișierul $suffix. Acest program este software gratuit, puteți redistribui și/sau să-l modificați în conformitate cu termenii licenței publice generale GNU publicată de către Fundația Software Liber versiunea 3 a licenței.

Acest program este distribuit în speranța că va fi util, dar fără nicio garanție, fără chiar garanția implicită de comercializare sau potrivire pentru un anumit scop. Vezi licența publică generală GNU pentru mai multe detalii.

Autor: Stefano Capitani
Stefano@manjaro.org Vizualizați și îmbinați fișierul $suffix Ce vrei să faci cu fișierul <span foreground='red'>$basefile</span>? 