��    
      l       �       �   �   �   
   �  -   �  %   �  A   �  *   )  �  T     -  O   M    �  �   �     �  8   �  %   �     �  2     �  N  #   0	  C   T	   <span foreground='red'>New Pacnew/Pacsave files found:\n</span>
Keep in mind:\n
You must be aware of your choices. If you are unsure please inquire using our social channels such as our support forum.\n Do nothing Keep the original and remove the $suffix file Management of Pacnew/Pacsave complete Now we will <span foreground='red'>remove</span> the $suffix file Replace the original with the $suffix file This program is free software, you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Author: Stefano Capitani
stefano@manjaro.org View and merge the $suffix file What do you want to do with the file <span foreground='red'>$basefile</span>?\n Project-Id-Version: manjaro-pacnew-checker
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 <span foreground='red'>Nuovi file pacnew/pacsave trovati:</span>
Ricorda:
devi essere consapevole delle tue scelte. Se non sei sicuro, chiedi informazioni utilizzando i nostri canali social come il nostro forum di supporto. Non fare nulla Mantenere il file originale e rimuovere il file $suffix. Gestione di pacnew/pacsave completata Ora rimuoveremo il file $suffix Sostituisci il file originale con il file $suffix. Questo programma è software libero, puoi ridistribuirlo e/o modificarlo in base ai termini della licenza pubblica generale GNU pubblicata dalla Free Software Foundation versione 3 della licenza.

Questo programma è distribuito nella speranza che sia utile, ma senza alcuna garanzia, neanche la garanzia implicita di commerciabilità o idoneità per uno scopo particolare. Vedi la licenza pubblica generale GNU per ulteriori dettagli.

Autore: Stefano Capitani
stefano@manjaro.org Visualizza e unisci il file $suffix Cosa vuoi fare con il file <span foreground='red'>$basefile</span>? 