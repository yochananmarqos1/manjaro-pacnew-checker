��    
      l       �       �   �   �   
   �  -   �  %   �  A   �  *   )  �  T     -  O   M    �  |  �     *  G   J  7   �  0   �  P   �  �  L  @   �  `   "   <span foreground='red'>New Pacnew/Pacsave files found:\n</span>
Keep in mind:\n
You must be aware of your choices. If you are unsure please inquire using our social channels such as our support forum.\n Do nothing Keep the original and remove the $suffix file Management of Pacnew/Pacsave complete Now we will <span foreground='red'>remove</span> the $suffix file Replace the original with the $suffix file This program is free software, you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Author: Stefano Capitani
stefano@manjaro.org View and merge the $suffix file What do you want to do with the file <span foreground='red'>$basefile</span>?\n Project-Id-Version: manjaro-pacnew-checker
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 <span foreground='red'>Найдены новые файлы pacnew/pacsave:</span>
Имейте в виду:
Вы должны быть осведомлены о своих выборах. Если вы не уверены, пожалуйста, задайте вопрос в наших социальных каналах, таких как наш форум поддержки. Ничего не делать. Сохраните оригинал и удалите файл $suffix. Управление pacnew/pacsave завершено. Теперь мы удалим файл $suffix. Замените исходный файл файлом с суффиксом $. Эта программа является свободным программным обеспечением, вы можете распространять ее и/или изменять ее в соответствии с условиями лицензии GNU General Public License, опубликованной Free Software Foundation версии 3 лицензии.

Эта программа распространяется с надеждой, что она будет полезна, но без каких-либо гарантий, даже без предполагаемой гарантии коммерческой ценности или пригодности для определенной цели. См. лицензию GNU General Public License для получения дополнительной информации.

Автор: Стефано Капитани
stefano@manjaro.org Просмотреть и объединить файл $suffix. Что вы хотите сделать с файлом <span foreground='red'>$basefile</span>? 