��    
      l       �       �   �   �   
   �  -   �  %   �  A   �  *   )  �  T     -  O   M    �  �   �     �  1   �  &   �  ?   
  '   J  �  r  $   U	  F   z	   <span foreground='red'>New Pacnew/Pacsave files found:\n</span>
Keep in mind:\n
You must be aware of your choices. If you are unsure please inquire using our social channels such as our support forum.\n Do nothing Keep the original and remove the $suffix file Management of Pacnew/Pacsave complete Now we will <span foreground='red'>remove</span> the $suffix file Replace the original with the $suffix file This program is free software, you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Author: Stefano Capitani
stefano@manjaro.org View and merge the $suffix file What do you want to do with the file <span foreground='red'>$basefile</span>?\n Project-Id-Version: manjaro-pacnew-checker
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 <span foreground='red'>Nové súbory pacnew/pacsave nájdené:</span>
Majte na pamäti:
Musíte byť si vedomí svojich voľieb. Ak ste si nie istí, obráťte sa na nás prostredníctvom našich sociálnych kanálov, ako je naše fórum podpory. Nerobte nič Uchovajte pôvodný a odstráňte súbor $suffix. Správa pacnew/pacsave je dokončená. Teraz <span foreground='red'>odstránime</span> súbor $suffix. Nahradiť pôvodný súbor s $suffixom. Tento program je voľne šíriteľný softvér, môžete ho redistribuovať a/alebo upravovať podľa podmienok GNU General Public License, ako bola vydaná Free Software Foundation verzia 3 licencie.

Tento program je šírený s nádejou, že bude užitočný, ale bez akýchkoľvek záruk, ani bez implicitnej záruky obchodovateľnosti alebo vhodnosti pre určitý účel. Pozrite si GNU General Public License pre viac podrobností.

Autor: Stefano Capitani
Stefano@manjaro.org Zobraziť a zlúčiť súbor $suffix Čo chceš urobiť s súborom <span foreground='red'>$basefile</span>? 