��    
      l       �       �   �   �   
   �  -   �  %   �  A   �  *   )  �  T     -  O   M    �  �   �  
   �  3   �  "   �  I   �  F   B  �  �  .   {	  H   �	   <span foreground='red'>New Pacnew/Pacsave files found:\n</span>
Keep in mind:\n
You must be aware of your choices. If you are unsure please inquire using our social channels such as our support forum.\n Do nothing Keep the original and remove the $suffix file Management of Pacnew/Pacsave complete Now we will <span foreground='red'>remove</span> the $suffix file Replace the original with the $suffix file This program is free software, you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Author: Stefano Capitani
stefano@manjaro.org View and merge the $suffix file What do you want to do with the file <span foreground='red'>$basefile</span>?\n Project-Id-Version: manjaro-pacnew-checker
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 <span foreground='red'>Nieuwe pacnew/pacsave-bestanden gevonden:\n</span>
Houd er rekening mee dat je bewust moet zijn van je keuzes. Als je je onzeker voelt, vraag dan gerust om hulp via onze sociale kanalen zoals ons supportforum. Niets doen Houd de originele en verwijder het $suffix bestand. Beheer van pacnew/pacsave voltooid Nu gaan we het $suffix bestand <span foreground='red'>verwijderen</span>. Vervang het originele bestand met het bestand met de $suffix-extensie. Dit programma is gratis software, u kunt het redistribueren en/of aanpassen onder de voorwaarden van de GNU General Public License zoals gepubliceerd door de Free Software Foundation versie 3 van de licentie.

Dit programma wordt verspreid in de hoop dat het nuttig zal zijn, maar zonder enige garantie, zelfs zonder de impliciete garantie van verkoopbaarheid of geschiktheid voor een bepaald doel. Zie de GNU General Public License voor meer details.

Auteur: Stefano Capitani
Stefano@manjaro.org Bekijk en samenvoeg het bestand met de $suffix Wat wil je doen met het bestand <span foreground='red'>$basefile</span>? 