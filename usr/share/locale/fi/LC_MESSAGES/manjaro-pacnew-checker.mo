��    
      l       �       �   �   �   
   �  -   �  %   �  A   �  *   )  �  T     -  O   M    �  �   �     �  /   �  1   �  >   �  2   4  �  g  "   7	  H   Z	   <span foreground='red'>New Pacnew/Pacsave files found:\n</span>
Keep in mind:\n
You must be aware of your choices. If you are unsure please inquire using our social channels such as our support forum.\n Do nothing Keep the original and remove the $suffix file Management of Pacnew/Pacsave complete Now we will <span foreground='red'>remove</span> the $suffix file Replace the original with the $suffix file This program is free software, you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Author: Stefano Capitani
stefano@manjaro.org View and merge the $suffix file What do you want to do with the file <span foreground='red'>$basefile</span>?\n Project-Id-Version: manjaro-pacnew-checker
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 <span foreground='red'>Uusia pacnew/pacsave-tiedostoja löytyi:\n</span>
Muista:\n
Sinun tulee olla tietoinen valinnoistasi. Jos olet epävarma, ota yhteyttä sosiaalisen median kanaviimme, kuten tukifoorumimme.\n Tehdä mitään Pidä alkuperäinen ja poista $suffix-tiedosto. Pacnew/pacsave-hallinnan suorittaminen on valmis. Nyt <span foreground='red'>poistamme</span> $suffix-tiedoston. Korvaa alkuperäinen tiedosto $suffix-tiedostolla. Tämä ohjelma on ilmainen ohjelmisto, voit jakaa sen ja/tai muokata sitä GNU General Public License -lisenssin ehtojen mukaisesti, jonka on julkaissut Free Software Foundation versiossa 3.

Tämä ohjelma jaetaan toivossa, että se olisi hyödyllinen, mutta ilman mitään takuuta, ilman edes hinnoittelutakuuta tai soveltuvuutta tiettyyn tarkoitukseen. Katso lisätietoja GNU General Public License -lisenssistä.

Tekijä: Stefano Capitani
Stefano@manjaro.org Katso ja yhdistä $suffix-tiedosto Mitä haluat tehdä tiedostolle <span foreground='red'>$basefile</span>? 