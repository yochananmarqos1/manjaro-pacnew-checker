��    
      l       �       �   �   �   
   �  -   �  %   �  A   �  *   )  �  T     -  O   M    �  �   �     p  &   �  )   �     �     �  �       �  D   �   <span foreground='red'>New Pacnew/Pacsave files found:\n</span>
Keep in mind:\n
You must be aware of your choices. If you are unsure please inquire using our social channels such as our support forum.\n Do nothing Keep the original and remove the $suffix file Management of Pacnew/Pacsave complete Now we will <span foreground='red'>remove</span> the $suffix file Replace the original with the $suffix file This program is free software, you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Author: Stefano Capitani
stefano@manjaro.org View and merge the $suffix file What do you want to do with the file <span foreground='red'>$basefile</span>?\n Project-Id-Version: manjaro-pacnew-checker
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 <span foreground='red'>Leiti uusi pacnew/pacsave faile:\n</span>
Ole ettevaatlik oma valikutega. Kui oled ebakindel, palun uuri meie sotsiaalmeedia kanalite, näiteks meie toetusfoorumi, abil.\n Mitte midagi teha Hoidke algne ja eemaldage $suffix fail Pacnew/pacsave'i haldamine on lõpetatud. Nüüd eemaldame $suffix faili. Asenda algne fail $suffixiga. See programm on vaba tarkvara, saate seda jaotada ja/või muuta seda vastavalt vaba tarkvara fondi väljastatud GNU üldise avaliku litsentsi tingimustele versioon 3.

Seda programmi levitatakse lootuses, et see on kasulik, kuid ilma igasuguse garantiita, isegi ilma kauplemiseks või teatud eesmärgiks sobivuse garantiita. Vaadake GNU üldise avaliku litsentsi üksikasju.

Autor: Stefano Capitani
Stefano@manjaro.org Vaata ja ühenda $suffix fail Mis te soovite teha failiga <span foreground='red'>$basefile</span>? 