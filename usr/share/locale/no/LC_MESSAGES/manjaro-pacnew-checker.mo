��    
      l       �       �   �   �   
   �  -   �  %   �  A   �  *   )  �  T     -  O   M    �  �   �     u  ,   �  *   �  =   �  +     �  F  #   .	  D   R	   <span foreground='red'>New Pacnew/Pacsave files found:\n</span>
Keep in mind:\n
You must be aware of your choices. If you are unsure please inquire using our social channels such as our support forum.\n Do nothing Keep the original and remove the $suffix file Management of Pacnew/Pacsave complete Now we will <span foreground='red'>remove</span> the $suffix file Replace the original with the $suffix file This program is free software, you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Author: Stefano Capitani
stefano@manjaro.org View and merge the $suffix file What do you want to do with the file <span foreground='red'>$basefile</span>?\n Project-Id-Version: manjaro-pacnew-checker
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 <span foreground='red'>Nye pacnew/pacsave-filer funnet:\n</span>
Husk på:
Du må være klar over dine valg. Hvis du er usikker, vennligst spørre via våre sosiale kanaler, som vårt støtteforum.\n Ikke gjør noe Behold det originale og fjern $suffix-filen. Administrering av pacnew/pacsave fullført Nå vil vi <span foreground='red'>fjerne</span> $suffix-filen Erstatt den opprinnelige med $suffix-filen. Dette programmet er gratis programvare, du kan redistribuere det og/eller endre det i henhold til vilkårene i GNU General Public License som publisert av Free Software Foundation versjon 3 av lisensen.

Dette programmet er distribuert i håp om at det vil være nyttig, men uten noen garanti, ikke engang den underforståtte garantien om salgbarhet eller egnethet for et bestemt formål. Se GNU General Public License for flere detaljer.

Forfatter: Stefano Capitani
Stefano@manjaro.org Se på og slå sammen $suffix-filen Hva vil du gjøre med filen <span foreground='red'>$basefile</span>? 