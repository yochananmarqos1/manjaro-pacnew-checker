��    
      l       �       �   �   �   
   �  -   �  %   �  A   �  *   )  �  T     -  O   M    �  �   �     �  '   �  %   �  !   �  %     �  <  "   	  D   &	   <span foreground='red'>New Pacnew/Pacsave files found:\n</span>
Keep in mind:\n
You must be aware of your choices. If you are unsure please inquire using our social channels such as our support forum.\n Do nothing Keep the original and remove the $suffix file Management of Pacnew/Pacsave complete Now we will <span foreground='red'>remove</span> the $suffix file Replace the original with the $suffix file This program is free software, you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Author: Stefano Capitani
stefano@manjaro.org View and merge the $suffix file What do you want to do with the file <span foreground='red'>$basefile</span>?\n Project-Id-Version: manjaro-pacnew-checker
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 <span foreground='red'>Nové soubory pacnew/pacsave nalezeny:\n</span>
Mějte na paměti:\n
Musíte být si vědomi svých volb. Pokud si nejste jisti, obraťte se prosím na naše sociální kanály, jako je naše podpůrné fórum.\n Nic nedělat Udržujte původní a odstraňte soubor Správa pacnew/pacsave je dokončena. Nyní odstraníme soubor $suffix. Nahrajte originální soubor $suffix. Tento program je svobodný software, můžete jej šířit a/nebo upravovat za podmínek GNU General Public License, jak byla publikována Free Software Foundation ve verzi 3 licence.

Tento program je šířen s nadějí, že bude užitečný, ale bez jakékoli záruky, ani bez záruky obchodovatelnosti nebo vhodnosti pro určitý účel. Podívejte se na GNU General Public License pro více podrobností.

Autor: Stefano Capitani
Stefano@manjaro.org Zobrazit a sloučit soubor $suffix Co chcete udělat s <span foreground='red'>$basefile</span> souboru? 