��    
      l       �       �   �   �   
   �  -   �  %   �  A   �  *   )  �  T     -  O   M    �  �  �     5  K   T  4   �  1   �  ;     `  C  @   �  ^   �   <span foreground='red'>New Pacnew/Pacsave files found:\n</span>
Keep in mind:\n
You must be aware of your choices. If you are unsure please inquire using our social channels such as our support forum.\n Do nothing Keep the original and remove the $suffix file Management of Pacnew/Pacsave complete Now we will <span foreground='red'>remove</span> the $suffix file Replace the original with the $suffix file This program is free software, you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Author: Stefano Capitani
stefano@manjaro.org View and merge the $suffix file What do you want to do with the file <span foreground='red'>$basefile</span>?\n Project-Id-Version: manjaro-pacnew-checker
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 <span foreground='red'>Знайдено нові файли pacnew/pacsave:</span>
Пам'ятайте:
Ви повинні бути обережними при виборі. Якщо ви не впевнені, будь ласка, зверніться до нас за допомогою наших соціальних каналів, таких як наш форум підтримки. Нічого не робити Зберігайте оригінал і видаліть файл $suffix. Керування pacnew/pacsave завершено Тепер ми видалимо файл $suffix Замініть оригінальний файл $suffix. Ця програма є вільним програмним забезпеченням, ви можете поширювати її та змінювати за умовами ліцензії GNU General Public License, опублікованої Фондом Вільного Програмного Забезпечення версії 3 ліцензії.

Ця програма розповсюджується з надією, що вона буде корисною, але без будь-яких гарантій, навіть без підтвердження гарантії продажу або придатності для певної мети. Дивіться ліцензію GNU General Public License для отримання додаткової інформації.

Автор: Стефано Капітані
Стефано@manjaro.org Перегляньте та об'єднайте файл $suffix Що ви хочете зробити з файлом <span foreground='red'>$basefile</span>? 