��    
      l      �       �   �   �   
   �  -   �  %   �  A     *   ]  �  �     a  O   �    �  �   �     �  1   �  $     S   C  +   �  �  �  $   �	  P   �	                               	      
        <span foreground='red'>New Pacnew/Pacsave files found:\n</span>
Keep in mind:\n
You must be aware of your choices. If you are unsure please inquire using our social channels such as our support forum.\n Do nothing Keep the original and remove the $suffix file Management of Pacnew/Pacsave complete Now we will <span foreground='red'>remove</span> the $suffix file Replace the original with the $suffix file This program is free software, you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Author: Stefano Capitani
stefano@manjaro.org View and merge the $suffix file What do you want to do with the file <span foreground='red'>$basefile</span>?\n Project-Id-Version: manjaro-pacnew-checker
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 <span foreground='red'>Nouveaux fichiers Pacnew/Pacsave trouvés:\n</span>
Gardez à l'esprit:\n
Vous devez être conscient de vos choix. Si vous n'êtes pas sûr, veuillez poser votre question sur nos canaux sociaux tel que notre forum d'assistance.\n Ne rien faire Garder l'original et supprimer le fichier $suffix Gestion des Pacnew/Pacsave terminée Maintenant, nous allons <span foreground='red'>supprimer</span> le fichier $suffix. Remplacer l'original par le fichier $suffix Ce programme est un logiciel libre, vous pouvez le redistribuer et/ou le modifier selon les termes de la licence publique générale GNU publiée par la Free Software Foundation version 3 de la licence.

Ce programme est distribué dans l'espoir qu'il sera utile, mais sans aucune garantie, sans même la garantie implicite de commercialisation ou d'adaptation à un usage particulier. Voir la licence publique générale GNU pour plus de détails.

Auteur : Stefano Capitani
Stefano@manjaro.org Voir et fusionner le fichier $suffix Que voulez-vous faire avec le fichier <span foreground='red'>$basefile</span>?\n 