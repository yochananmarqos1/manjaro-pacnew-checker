��    
      l      �       �   �   �   
   �  -   �  %   �  A     *   ]  �  �     a  O   �    �  �   �     �  :   �  #     C   <  4   �  �  �  !   �	  M   �	                               	      
        <span foreground='red'>New Pacnew/Pacsave files found:\n</span>
Keep in mind:\n
You must be aware of your choices. If you are unsure please inquire using our social channels such as our support forum.\n Do nothing Keep the original and remove the $suffix file Management of Pacnew/Pacsave complete Now we will <span foreground='red'>remove</span> the $suffix file Replace the original with the $suffix file This program is free software, you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Author: Stefano Capitani
stefano@manjaro.org View and merge the $suffix file What do you want to do with the file <span foreground='red'>$basefile</span>?\n Project-Id-Version: manjaro-pacnew-checker
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 <span foreground='red'>Se encontraron nuevos archivos pacnew/pacsave:\n</span>
Tenga en cuenta:

Debe ser consciente de sus elecciones. Si no está seguro, pregunte usando nuestros canales sociales, por ejemplo nuestro foro de soporte.\n No hacer nada Mantener el archivo original y eliminar el archivo $suffix Gestión de Pacnew/Pacsave completa Ahora <span foreground='red'>eliminaremos</span> el archivo $suffix Sustituir el archivo original por el archivo $suffix Este programa es software libre, puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General GNU publicada por la Free Software Foundation, versión 3 de la licencia.

Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, ni siquiera la garantía implícita MERCANTIL o ADECUACIÓN PARA UN PROPÓSITO PARTICULAR. Consulte la Licencia Pública General GNU para más detalles.

Autor: Stefano Capitani
Stefano@manjaro.org Ver y fusionar el archivo $suffix ¿Qué quiere hacer con el archivo <span foreground='red'>$basefile</span>?\n 