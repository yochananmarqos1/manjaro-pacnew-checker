��    
      l      �       �   �   �   
   �  -   �  %   �  A     *   Z  �  �     ^  N   ~    �  �  �     k  N   �  B   �  Q     F   h    �  9   /  g   i                               	      
        <span foreground='red'>New Pacnew/Pacsave files found:
</span>
Keep in mind:

You must be aware of your choices. If you are unsure please inquire using our social channels such as our support forum.
 Do nothing Keep the original and remove the $suffix file Management of Pacnew/Pacsave complete Now we will <span foreground='red'>remove</span> the $suffix file Replace the original with the $suffix file This program is free software, you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Author: Stefano Capitani
stefano@manjaro.org View and merge the $suffix file What do you want to do with the file <span foreground='red'>$basefile</span>?
 Project-Id-Version: manjaro-pacnew-checker
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 <span foreground='red'>Намерени са нови pacnew/pacsave файлове:</span>
Имайте предвид:
Трябва да бъдете внимателни при избора си. Ако не сте сигурни, моля задайте въпрос през нашите социални канали, като например нашия форум за поддръжка.
 Не прави нищо Запазете оригинала и премахнете файла $suffix Управлението на pacnew/pacsave е завършено Сега ще <span foreground='red'>премахнем</span> файла $suffix Заменете оригиналния файл с файла $suffix Тази програма е свободен софтуер, можете да я разпространявате и/или да я модифицирате под условията на Общото публично лицензиране GNU версия 3 (GNU General Public License), както е публикувано от Фондацията за свободен софтуер.

Тази програма се разпространява с надеждата, че ще бъде полезна, но без каквито и да било гаранции, включително гаранция за търговска пригодност или пригодност за определена цел. Вижте Общото публично лицензиране GNU за повече подробности.

Автор: Стефано Капитани
stefano@manjaro.org Разгледайте и слейте файла $suffix Какво искате да направите с файла <span foreground='red'>$basefile</span>?
 