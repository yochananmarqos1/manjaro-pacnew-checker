��    
      l       �       �   �   �   
   �  -   �  %   �  A   �  *   )  �  T     -  O   M    �  �   �     �  -   �  (   �  C   �  '   ;  �  c  %   .	  J   T	   <span foreground='red'>New Pacnew/Pacsave files found:\n</span>
Keep in mind:\n
You must be aware of your choices. If you are unsure please inquire using our social channels such as our support forum.\n Do nothing Keep the original and remove the $suffix file Management of Pacnew/Pacsave complete Now we will <span foreground='red'>remove</span> the $suffix file Replace the original with the $suffix file This program is free software, you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Author: Stefano Capitani
stefano@manjaro.org View and merge the $suffix file What do you want to do with the file <span foreground='red'>$basefile</span>?\n Project-Id-Version: manjaro-pacnew-checker
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 <span foreground='red'>Pronađeni novi pacnew/pacsave datoteke:</span>
Imajte na umu:
Morate biti svjesni svojih odabira. Ako niste sigurni, molimo Vas da se obratite našim društvenim kanalima, poput našeg podrška foruma. Ništa ne raditi Drži izvornik i ukloni datoteku s $suffixom. Upravljanje pacnew/pacsave je dovršeno. Sada ćemo <span foreground='red'>ukloniti</span> datoteku $suffix. Zamijenite izvorni s $suffix datotekom. Ovaj program je besplatan softver, možete ga redistribuirati i/ili mijenjati prema uvjetima GNU Opće javne licence koju je objavila Udruga za slobodni softver, verzija 3 licence.

Ovaj program je distribuiran u nadi da će biti koristan, ali bez ikakve garancije, čak i bez implicitne garancije trgovačke vrijednosti ili prilagođenosti za određenu svrhu. Pogledajte GNU Opću javnu licencu za više detalja.

Autor: Stefano Capitani
Stefano@manjaro.org Pogledajte i spojite datoteku $suffix Što želite učiniti s datotekom <span foreground='red'>$basefile</span>? 