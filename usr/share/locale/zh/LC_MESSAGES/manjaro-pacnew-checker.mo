��    
      l       �       �   �   �   
   �  -   �  %   �  A   �  *   )  �  T     -  O   M    �  �   �     {  %   �  )   �  C   �  R     �  r     �  K   	   <span foreground='red'>New Pacnew/Pacsave files found:\n</span>
Keep in mind:\n
You must be aware of your choices. If you are unsure please inquire using our social channels such as our support forum.\n Do nothing Keep the original and remove the $suffix file Management of Pacnew/Pacsave complete Now we will <span foreground='red'>remove</span> the $suffix file Replace the original with the $suffix file This program is free software, you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Author: Stefano Capitani
stefano@manjaro.org View and merge the $suffix file What do you want to do with the file <span foreground='red'>$basefile</span>?\n Project-Id-Version: manjaro-pacnew-checker
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 <span foreground='red'>发现新的pacnew/pacsave文件：</span>
请记住：
你必须意识到自己的选择。如果你不确定，请使用我们的社交渠道，如我们的支持论坛来查询。 什么也不做 保持原有，删除$suffix文件。 pacnew/pacsave文件的管理完成了。 现在我们将<span foreground='red'>移除</span>$suffix文件。 原始文件被$suffix文件所取代。

原始文件被$suffix文件所取代。 这个程序是自由软件，您可以根据自由软件基金会发布的GNU通用公共许可证（GPL）第3版的条款重新分发和/或修改它。

本程序是希望它有用，但没有任何保证，甚至没有商业性或适合特定目的的默示保证。有关更多详细信息，请参阅GNU通用公共许可证。

作者：斯特凡诺·卡皮塔尼
斯特凡诺@manjaro.org 查看并合并$suffix文件 你想要如何处理<span foreground='red'>$basefile</span>这个文件？ 