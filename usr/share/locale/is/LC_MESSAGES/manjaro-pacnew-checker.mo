��    
      l       �       �   �   �   
   �  -   �  %   �  A   �  *   )  �  T     -  O   M    �    �     �  0   �        E      >   f  �  �  !   �	  L   �	   <span foreground='red'>New Pacnew/Pacsave files found:\n</span>
Keep in mind:\n
You must be aware of your choices. If you are unsure please inquire using our social channels such as our support forum.\n Do nothing Keep the original and remove the $suffix file Management of Pacnew/Pacsave complete Now we will <span foreground='red'>remove</span> the $suffix file Replace the original with the $suffix file This program is free software, you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Author: Stefano Capitani
stefano@manjaro.org View and merge the $suffix file What do you want to do with the file <span foreground='red'>$basefile</span>?\n Project-Id-Version: manjaro-pacnew-checker
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 <span foreground='red'>Fannst nýjar pacnew/pacsave skrár:</span>
Hafðu í huga:
Þú verður að vera meðvitaður um valið þitt. Ef þú ert ekki viss, vinsamlegast fáðu upplýsingar með því að nota samfélagsmiðla okkar eins og stuðningsspjallborðið okkar. Að gera ekkert Maintaen an táirge $suffix

Maintaen an táirge Stjórnun pacnew/pacsave lokið አሁን እኛ የ$suffix ፋይል ማስወገድ ይችላሉ። Ert þú að leita að upprunalegu skrá með $suffix endingu? Þessi forrit er frjáls hugbúnaður, þú getur endurtekið hann og/eða breytt honum í samræmi við GNU almenna notkunarleyfið sem birt er af Frjáls hugbúnaðarstofnuninni útgáfa 3 af leyfinu.

Þessi forrit er dreift með von um að það verði notað, en án neinna öryggisvottorða, jafnvel án þess ímyndaða öryggisvottorða sem kemur fram í kaup og sel. Sjáðu GNU almenna notkunarleyfið fyrir fleiri upplýsingar.

Höfundur: Stefano Capitani
Stefano@manjaro.org Skoða og sameina $suffix skrána ¿Qué quieres hacer con el archivo <span foreground='red'>$basefile</span>? 