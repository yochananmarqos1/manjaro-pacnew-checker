��    
      l       �       �   �   �   
   �  -   �  %   �  A   �  *   )  �  T     -  O   M    �  �  �     L  W   h  G   �  ]     N   f  �  �  H     c   �   <span foreground='red'>New Pacnew/Pacsave files found:\n</span>
Keep in mind:\n
You must be aware of your choices. If you are unsure please inquire using our social channels such as our support forum.\n Do nothing Keep the original and remove the $suffix file Management of Pacnew/Pacsave complete Now we will <span foreground='red'>remove</span> the $suffix file Replace the original with the $suffix file This program is free software, you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Author: Stefano Capitani
stefano@manjaro.org View and merge the $suffix file What do you want to do with the file <span foreground='red'>$basefile</span>?\n Project-Id-Version: manjaro-pacnew-checker
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 <span foreground='red'>Βρέθηκαν νέα αρχεία pacnew/pacsave:</span>
Να έχετε υπόψη σας:
Πρέπει να είστε ευθύνης για τις επιλογές σας. Εάν δεν είστε σίγουροι, παρακαλούμε να ζητήσετε βοήθεια μέσω των κοινωνικών δικτύων μας, όπως το φόρουμ υποστήριξης. Καμία ενέργεια Κρατήστε το αρχικό και αφαιρέστε το αρχείο $suffix. Η διαχείριση των pacnew/pacsave ολοκληρώθηκε. Τώρα θα <span foreground='red'>αφαιρέσουμε</span> το αρχείο $suffix. Αντικαταστήστε το αρχικό με το αρχείο $suffix. Αυτό το πρόγραμμα είναι ελεύθερο λογισμικό, μπορείτε να το αναδιανείμετε και/ή να το τροποποιήσετε σύμφωνα με τους όρους της άδειας γενικής δημόσιας άδειας GNU όπως δημοσιεύτηκε από το Ίδρυμα Ελεύθερου Λογισμικού, έκδοση 3 της άδειας.

Αυτό το πρόγραμμα διανέμεται με την ελπίδα ότι θα είναι χρήσιμο, αλλά χωρίς καμία εγγύηση, χωρίς καν η υπονοούμενη εγγύηση εμπορικής ή κατάλληλης για κάποιο συγκεκριμένο σκοπό. Δείτε την άδεια γενικής δημόσιας άδειας GNU για περισσότερες λεπτομέρειες.

Συγγραφέας: Στέφανο Καπιτάνι
Στέφανο@manjaro.org Προβολή και συγχώνευση του αρχείου $suffix Τι θέλετε να κάνετε με το αρχείο <span foreground='red'>$basefile</span>; 