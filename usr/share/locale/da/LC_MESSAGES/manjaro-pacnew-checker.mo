��    
      l       �       �   �   �   
   �  -   �  %   �  A   �  *   )  �  T     -  O   M    �  �   �  	   �  -   �  #   �  <   �  *   -  �  X     9	  K   R	   <span foreground='red'>New Pacnew/Pacsave files found:\n</span>
Keep in mind:\n
You must be aware of your choices. If you are unsure please inquire using our social channels such as our support forum.\n Do nothing Keep the original and remove the $suffix file Management of Pacnew/Pacsave complete Now we will <span foreground='red'>remove</span> the $suffix file Replace the original with the $suffix file This program is free software, you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Author: Stefano Capitani
stefano@manjaro.org View and merge the $suffix file What do you want to do with the file <span foreground='red'>$basefile</span>?\n Project-Id-Version: manjaro-pacnew-checker
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 <span foreground='red'>Nye pacnew/pacsave-filer fundet:\n</span>
Husk på:\n
du skal være opmærksom på dine valg. Hvis du er usikker, skal du venligst forespørge ved hjælp af vores sociale kanaler såsom vores supportforum.\n Ingenting Hold den oprindelige og fjern den $suffix-fil Styring af pacnew/pacsave fuldført Nu vil vi <span foreground='red'>fjerne</span> $suffix-filen Erstat den oprindelige med den $suffix-fil Dette program er gratis software, du kan redistribuere det og/eller ændre det i henhold til betingelserne i GNU General Public License som er udgivet af Free Software Foundation version 3 af licensen.

Dette program distribueres i håb om, at det vil være nyttigt, men uden nogen garanti, uden endda en underforstået garanti for salgbarhed eller egnethed til et bestemt formål. Se GNU General Public License for flere detaljer.

Forfatter: Stefano Capitani
Stefano@manjaro.org Se og flet $suffix-filen Hvad ønsker du at gøre med filen <span foreground='red'>$basefile</span>? 