# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the manjaro-pacnew-checker package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: manjaro-pacnew-checker\n"
"Report-Msgid-Bugs-To: \n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: attranslate\n"

msgid "Now we will <span foreground='red'>remove</span> the $suffix file"
msgstr "Sada ćemo <span foreground='red'>ukloniti</span> datoteku $suffix."

msgid ""
"This program is free software, you can redistribute it and/or modify it under the terms of the GNU "
"General Public License as published by the Free Software Foundation version 3 of the License.\n"
"\n"
"This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, without "
"even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU "
"General Public License for more details.\n"
"\n"
"Author: Stefano Capitani\n"
"stefano@manjaro.org"
msgstr ""
"Ovaj program je besplatan softver, možete ga redistribuirati i/ili mijenjati prema uvjetima GNU "
"Opće javne licence koju je objavila Udruga za slobodni softver, verzija 3 licence.\n"
"\n"
"Ovaj program je distribuiran u nadi da će biti koristan, ali bez ikakve garancije, čak i bez "
"implicitne garancije trgovačke vrijednosti ili prilagođenosti za određenu svrhu. Pogledajte GNU "
"Opću javnu licencu za više detalja.\n"
"\n"
"Autor: Stefano Capitani\n"
"Stefano@manjaro.org"

msgid ""
"<span foreground='red'>New Pacnew/Pacsave files found:\\n"
"</span>\n"
"Keep in mind:\\n"
"\n"
"You must be aware of your choices. If you are unsure please inquire using our social channels such "
"as our support forum.\\n"
msgstr ""
"<span foreground='red'>Pronađeni novi pacnew/pacsave datoteke:</span>\n"
"Imajte na umu:\n"
"Morate biti svjesni svojih odabira. Ako niste sigurni, molimo Vas da se obratite našim društvenim "
"kanalima, poput našeg podrška foruma."

msgid "What do you want to do with the file <span foreground='red'>$basefile</span>?\\n"
msgstr "Što želite učiniti s datotekom <span foreground='red'>$basefile</span>?"

msgid "Keep the original and remove the $suffix file"
msgstr "Drži izvornik i ukloni datoteku s $suffixom."

msgid "View and merge the $suffix file"
msgstr "Pogledajte i spojite datoteku $suffix"

msgid "Replace the original with the $suffix file"
msgstr "Zamijenite izvorni s $suffix datotekom."

msgid "Do nothing"
msgstr "Ništa ne raditi"

msgid "Management of Pacnew/Pacsave complete"
msgstr "Upravljanje pacnew/pacsave je dovršeno."
