# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the manjaro-pacnew-checker package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: manjaro-pacnew-checker\n"
"Report-Msgid-Bugs-To: \n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: attranslate\n"

msgid "Now we will <span foreground='red'>remove</span> the $suffix file"
msgstr "Nüüd eemaldame $suffix faili."

msgid ""
"This program is free software, you can redistribute it and/or modify it under the terms of the GNU "
"General Public License as published by the Free Software Foundation version 3 of the License.\n"
"\n"
"This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, without "
"even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU "
"General Public License for more details.\n"
"\n"
"Author: Stefano Capitani\n"
"stefano@manjaro.org"
msgstr ""
"See programm on vaba tarkvara, saate seda jaotada ja/või muuta seda vastavalt vaba tarkvara fondi "
"väljastatud GNU üldise avaliku litsentsi tingimustele versioon 3.\n"
"\n"
"Seda programmi levitatakse lootuses, et see on kasulik, kuid ilma igasuguse garantiita, isegi ilma "
"kauplemiseks või teatud eesmärgiks sobivuse garantiita. Vaadake GNU üldise avaliku litsentsi "
"üksikasju.\n"
"\n"
"Autor: Stefano Capitani\n"
"Stefano@manjaro.org"

msgid ""
"<span foreground='red'>New Pacnew/Pacsave files found:\\n"
"</span>\n"
"Keep in mind:\\n"
"\n"
"You must be aware of your choices. If you are unsure please inquire using our social channels such "
"as our support forum.\\n"
msgstr ""
"<span foreground='red'>Leiti uusi pacnew/pacsave faile:\\n"
"</span>\n"
"Ole ettevaatlik oma valikutega. Kui oled ebakindel, palun uuri meie sotsiaalmeedia kanalite, "
"näiteks meie toetusfoorumi, abil.\\n"

msgid "What do you want to do with the file <span foreground='red'>$basefile</span>?\\n"
msgstr "Mis te soovite teha failiga <span foreground='red'>$basefile</span>?"

msgid "Keep the original and remove the $suffix file"
msgstr "Hoidke algne ja eemaldage $suffix fail"

msgid "View and merge the $suffix file"
msgstr "Vaata ja ühenda $suffix fail"

msgid "Replace the original with the $suffix file"
msgstr "Asenda algne fail $suffixiga."

msgid "Do nothing"
msgstr "Mitte midagi teha"

msgid "Management of Pacnew/Pacsave complete"
msgstr "Pacnew/pacsave'i haldamine on lõpetatud."
