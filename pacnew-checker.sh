#!/bin/bash

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# Author: Stefano Capitani <stefanoatmanjarodotorg>

#Translation
export TEXTDOMAINDIR="/usr/share/locale"
export TEXTDOMAIN=manjaro-pacnew-checker

export PATH=/usr/bin:/usr/sbin

conflict_files=$(pacdiff -o)
icon=/usr/share/icons/manjaro/maia/32x32.png
title='Pacdiff Checker'
title1='Management of Pacnew/Pacsave files'

files=$(mktemp)
pacdiff -o > "$files"

trap "rm -f ""$files"" ; exit" ERR EXIT

merge_function() {
    #Opening meld
    /usr/bin/meld "admin://${file%."$suffix"}" "admin://$file"
    yad --title="$title1" --image="$icon" --text=$"Now we will <span foreground='red'>remove</span> the $suffix file" \
    --button=Cancel:1 --button=Ok:0

    a="$?"

    if [[ "$a" -eq "0" ]]; then
        pkexec rm -f "$file" /etc/pacnew-check.file
    fi
}


wiki_link() {
    xdg-open https://wiki.manjaro.org/index.php/System_Maintenance/en#Pacnew_and_Pacsave_files 
}

forum_link() {
    xdg-open https://forum.manjaro.org/
}

disclaimer_function() {
    yad --title="$title" --use-interp --width=750 --text=$"This program is free software, you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Author: Stefano Capitani
stefano@manjaro.org" --button=Ok

}

export -f forum_link
export -f wiki_link
export -f merge_function
export -f disclaimer_function

#Check if files array is empty and exit
if [[ -z $(cat "$files") ]]; then
    exit 1
fi

yad --title="$title" --center --use-interp --width=500 --on-top --info --text-info < "$files" --image="$icon" --text=$"<span foreground='red'>New Pacnew/Pacsave files found:\n</span>
Keep in mind:\n
You must be aware of your choices. If you are unsure please inquire using our social channels such as our support forum.\n" --button=Disclaimer:"disclaimer_function" --button=Wiki:"wiki_link" --button=Forum:"forum_link" --button=Ok:0

#Repeat through the conflict files and allow the user to choose what to do
for file in $conflict_files; do

    suffix="${file##*.}"
    basefile="$(basename $file)"

    choices=$(yad --title="$title1" --use-interp --height=200 --width=500 --text=$"What do you want to do with the file <span foreground='red'>$basefile</span>?\n" \
    --list --no-headers --column=Option --column=Action \
    1 $"Keep the original and remove the $suffix file" \
    2 $"View and merge the $suffix file" \
    3 $"Replace the original with the $suffix file" \
    4 $"Do nothing" \
    --no-buttons)
    
    choice=$(echo $choices | cut -d '|' -f1)
    
    case "$choice" in
        '1')
            # The user has chosen to keep the original file, do not do anything and remove the Pac file.
            pkexec rm -f "$file" /etc/pacnew-check.file ;;
        '2')
            # The user has chosen to compare files.
            merge_function ;;
        '3')
            # The user has chosen to replace the current configuration file with the Pac file.
            pkexec mv -f "$file" "${file%."$suffix"}" && pkexec rm -f /etc/pacnew-check.file ;;
        '4')
            continue  ;;
    esac
done

yad --title="Pacdiff Checker" --info --image="$icon" --text=$"Management of Pacnew/Pacsave complete" --button=Done

unset -f merge_function
unset -f forum_link
unset -f wiki_link
unset -f disclaimer_function

[[ -f /etc/pacnew-check.file ]] && pkexec rm -f /etc/pacnew-check.file
 
#Remove this file if present ( it comes from v0.3.2 )
[[ -f $HOME/.config/.check-pacnew ]] && rm -f $HOME/.config/.check-pacnew

#End
